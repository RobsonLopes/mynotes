var app = angular.module("mynotes", ['ngRoute']).config(function($routeProvider){
    $routeProvider
    .when('/list', {
        templateUrl: 'view/list.html',
        controller: 'getList'
    })
    .when('/add', {
        templateUrl: 'view/add.html',
        controller: 'addNote'
    })
    .when('/show/:id', {
        templateUrl: 'view/show.html',
        controller: 'showNote'
    })
    .otherwise({
        redirectTo: '/list'
    });
});
