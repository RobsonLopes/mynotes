<?php
    include("connect.php");
    $return = [];
    
    $sql = $con->query("SELECT * FROM notes");
    while($row = $sql->fetch_assoc()) {
        $return[$row['id']]['id'] = $row['id'];
        $return[$row['id']]['title'] = $row['title'];
        $return[$row['id']]['content'] = $row['content'];
        $return[$row['id']]['date'] = $row['date'];
    }
    $return = json_encode($return);
    echo $return;
?>