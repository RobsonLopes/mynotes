<?php
    include("connect.php");

    if(isset($_POST['title']) && isset($_POST['content'])){
        $title = $con->real_escape_string($_POST['title']);
        $content = $con->real_escape_string($_POST['content']);
        $date = date('Y-m-d H:i:s');
        
        $sql = $con->query("INSERT INTO notes (title, content, date) VALUES('$title','$content', '$date')");
        
        if($sql){
            $resp = ['status' => 'success', 'msg' => 'Adicionado com sucesso!'];
            echo json_encode($resp);
        }else{
            $resp = ['status' => 'error', 'msg' => 'Erro de SQL!'];
            echo json_encode($resp);
        }
    }else{
        $resp = ['status' => 'error', 'msg' => 'Você deve preencher os campos corretamente!'];
        echo json_encode($resp);
    }   
?>