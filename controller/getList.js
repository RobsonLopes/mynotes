app.controller('getList', function($scope, $routeParams, List){
    List.getLista(function(data){
        //// Troca espaço por "T" na data para funcionar o filtro na view
        angular.forEach(data, function(value, key){
            value.date = value.date.replace(" ", "T");
        }, data);
        
        $scope.itens = data;        
    });
});

app.service('List', function($http){
    var lista;
    var obj = {};
    var urljSon = "upload/getList.php";
    
    /// Pega a lista de notes do JSON
    obj = {
        getLista: function(callback){
            $http({
                method: 'POST',
                url: urljSon
            }).success(function(data){
                callback(data);
            });
        }
    };
    
    return obj;
});