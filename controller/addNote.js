app.controller('addNote', function($scope, $routeParams, $http, $httpParamSerializerJQLike){
    $scope.submit = function(evt){
        var urljSon = "upload/addList.php";
//        console.log(this);
        console.log($scope.addingform);

        $http({
            method: "POST",
            url: urljSon,
            data: $httpParamSerializerJQLike({
                title : this.title,
                content : this.content
            }),
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8;'
            }
        }).success(function(resp){
            if(resp.status == 'success'){
                console.log('here');
                alert(resp.msg);
            }else if(resp.status == 'error'){
                alert(resp.msg);
                $scope.addingform.$setPristine(true);
                $scope.addingform.$setUntouched(true);
//                $scope.$apply();
            }else{
                alert('Erro desconhecido!');
            }
        });
    }
});